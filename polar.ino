#include <Stepper.h>

const int step_360 = 20;// 360 number of steps per/rev                               
float revo1 = 5.0;
float revo2 = 5.0;
float max_loop;
int motor1_position = 0;
int motor2_position = 0;
int prev_motor1_position = 0;
int prev_motor2_position = 0;
int interval1 = 0;
int interval2 = 0;

// initialize the stepper library on pins 2-5 n 8-11 
Stepper myStepper1(step_360,2,3,4,5);
Stepper myStepper2(step_360,6,7,8,9);       
int dir1_rotation = 1;
int dir2_rotation = 1;

void setup() 
{
 // set the speed at 60 rpm:
 if(revo1 > revo2) {
   max_loop = revo1;
    interval1 = 1.0;
    interval2 = revo1 - revo2 + 1;
 } else {
   max_loop = revo2;
    interval1 = revo2 - revo1 +1;
    interval2 = 1.0;
 }
 myStepper1.setSpeed(60);//left
 myStepper2.setSpeed(60);//right
 // initialize the serial port:
 Serial.begin(9600);
 Serial.print(interval1);
 Serial.print(" - interval - ");
 Serial.println(interval2);
}
void loop() 
{
  Serial.println("Start");
  if(motor1_position < max_loop * step_360) {
    if(motor1_position - prev_motor1_position >= interval1) {
      myStepper1.step(dir1_rotation);
      prev_motor1_position = motor1_position;
    }
    if(motor2_position - prev_motor2_position >= interval2) {
      myStepper2.step(dir2_rotation);
      prev_motor2_position = motor2_position;
    }
    
    motor1_position = motor1_position + 1;
    
    motor2_position = motor2_position + 1;
  } else {
    dir1_rotation = -1 * dir1_rotation;
    motor1_position = 0;
    prev_motor1_position = 0;

    dir2_rotation = -1 * dir2_rotation;
    motor2_position = 0;
    prev_motor2_position = 0;
  }

  Serial.println('.');
  //delay(1000);
}
